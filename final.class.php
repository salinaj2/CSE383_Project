<?php 
class final_rest
{
 public static function setLookup ($value)
 {
	 if (!isset($value)) {
		 $retData["status"]=1;
		 $retData["message"]="'$value' is not set";
	 }
	 else {
		 try {
			 EXEC_SQL("insert into lookup (value, date) values (?,CURRENT_TIMESTAMP)", $value);
			 $retData["status"]=0;
			 $retData["message"]="insert of '$value' accepted";
		 }
		 catch  (Exception $e) {
			 $retData["status"]=1;
			 $retData["message"]=$e->getMessage();
		 }
	 }

	 return json_encode ($retData);
 }

 public static function getLookup ($date)
 {
	 if (!isset($date)) {
		 $retData["status"]=1;
		 $retData["result"]="'$date' is not set";
	 }
	 else {
		 try {
			 $retData["status"]=0;
			 $retData["result"] = GET_SQL("select * from lookup where date like ? order by date", $date."%");

		 }
		 catch  (Exception $e) {
			 $retData["status"]=1;
			 $retData["result"]=$e->getMessage();
		 }
	 }

	 return json_encode ($retData);
 }
}
