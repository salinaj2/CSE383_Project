var containerHtml = '<table class="table"><thead class="fontColor"><tr><th scope="col">Date</th><th scope="col">Time</th><th scope="col">From</th>					<th scope="col">To</th>					<th scope="col">Number of maneuvers</th></tr></thead>	<tbody class="fontColor" id="tableBody"></tbody></table>'
var objArr= [];
function recieveLocation() {
	srcAddress = $("#srcAddr").val();	
	srcCity = $("#srcCity").val();
	srcState = $("#srcState").val();
	srcZip = $("#srcZip").val();

	source = srcAddress + "," + srcCity + "," + srcState + "," + srcZip;
	destAddress = $("#destAddr").val();	
	destCity = $("#destCity").val();
	destState = $("#destState").val();
	destZip = $("#destZip").val();

	destination = destAddress + "," + destCity + "," + destState + "," + destZip;
	url = "http://www.mapquestapi.com/directions/v2/route?key=qOGMFAlI4vlRNDK9a3NL24KrLrc4LXHJ&from=" + source + "&to="+ destination;
	$.ajax({
		method: "GET",
		url: url
	}).done(function(data) {
		console.log(data);
		if (data.info.statuscode != 0) {
			console.log(data.info);
			insideHtml = "<div class=''><h1 class='fontColor centerText'>" + data.info.messages[0] + "</h1></div>";
			$("#dirContainer").html(insideHtml);
			$("#dirContainer").removeClass("d-none");
			return;
		}
		sendInformation(data);
		getElevation(data.route.locations[0].latLng, data.route.locations[1].latLng);
		directionArr = data.route.legs[0].maneuvers
		insideHtml = "";
		for (const element of directionArr) {
			insideHtml += "<div class='row justify-content-center mb-2'><div class='col-md-3'><img src='" + element.mapUrl + "' onerror=this.src='" + element.iconUrl + "' width='150' height='100'></div><div class='fontColor col-md-3'>" + element.narrative + "</div><div class='fontColor col-md-3'>" + element.formattedTime + " </div><div class='fontColor col-md-3'> " + element.distance+ " miles </div></div>";

		}
		$("#dirContainer").html(insideHtml);
		$("#dirContainer").removeClass("d-none");
		
	}).fail(function(error) {

	});
}

function getElevation(source, destination) {
	console.log(source)
	console.log(destination);
	url ="http://open.mapquestapi.com/elevation/v1/chart?key=qOGMFAlI4vlRNDK9a3NL24KrLrc4LXHJ&width=425&height=350&latLngCollection=" + source.lat + "," + source.lng + "," + destination.lat + "," + destination.lng;
	$("#heightContainer").html("<img src=" + url + ">");
	$("#heightContainer").removeClass("d-none");
}
function sendInformation(data) {
	sData = JSON.stringify(data);
	sendData = {value: sData};
	urlMethod = "http://salinaj2.aws.csi.miamioh.edu/final.php?method=setLookup";
	$.ajax({
		url: urlMethod,
		method: "POST",
		data: sendData
	}).done(function (data) {
		console.log(data);
	}).fail(function(err) {
		console.log(err);
	});
}

function getInformation() {
	date = $("#date").val();
	urlMethod = "http://salinaj2.aws.csi.miamioh.edu/final.php?method=getLookup&date=" + date;

	$.ajax({
		url: urlMethod,
		method: "GET"
	}).done(function (data) {
		resultArray = data.result;
		if (data.errorMessage) {
			console.log(data.errorMessage);
			return;
		}
		$("#tableBody").html("");
		numCol = $("#num").val();
		objArr = [];
		i = 0;
		console.log(resultArray);
		$("#main").html(containerHtml);
		inHtml = "";
		resultArray.every( (obj) => {
			if (i >= numCol || i >= resultArray.length) {
				console.log(resultArray.length);
				return false;
			}
			obj = JSON.parse(obj.value);
			objArr.push(obj);
			time = obj.route.legs[0].formattedTime
			location1 = obj.route.locations[0];
			location2 = obj.route.locations[1];
			from = location1.street + " " + location1.adminArea5 + " "+ location1.adminArea3;
			to = location2.street + " " + location2.adminArea5 + " " + location2.adminArea3;
			manuevers = obj.route.legs[0].maneuvers.length;
			inHtml += "<tr onclick='getDatabaseDirections( +" + i + ")'><td>" + $("#date").val() + "</td><td>" + time + "</td><td>" + from + "</td><td>" + to + "</td><td>" + manuevers + "</td></tr>";
			i++;
			return true;
		});
		$("#tableBody").html(inHtml);

		$("#main").removeClass("d-none");
	}).fail(function (err) {
		console.log(err);
	});
}
function getDatabaseDirections(index) {
	directionArr = objArr[index].route.legs[0].maneuvers;
	insideHtml = "";
	for (const element of directionArr) {
			insideHtml += "<div class='row justify-content-center mb-2'><div class='col-md-3'><img src='" + element.mapUrl + "' onerror=this.src='" + element.iconUrl + "' width='100' height='100'></div><div class='fontColor col-md-3'>" + element.narrative + "</div><div class='fontColor col-md-3'>" + element.formattedTime + " </div><div class='fontColor col-md-3'> " + element.distance+ " miles </div></div>";
		console.log(element.iconUrl);
	}

	$("#main").html(insideHtml);

}
